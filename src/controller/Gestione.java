package controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import model.Officina;
import model.Direttore;
import model.Dipendente;
import model.ContrattoDire;
import model.ContrattoDip;
import model.Fattura;
import model.Riparazione;
import model.Veicolo;
import model.Propietario;


public class Gestione {
	
	public Gestione() {
		
	};
	
	
	////////////////////////ARRAYLIST E METODI DIRETTORI//////////////////////
	
	//static Date dataDirettori = new Date();
	
	ArrayList<Direttore> elencoDirettori = new ArrayList<Direttore>();
	
	public ArrayList<Direttore> getElencoDirettori(){
		
		return(elencoDirettori);
		
	};
	
	public void setDirettori(Direttore dirigente,String codiceFiscale, String residenza, String numeroDiTelefono, String dataDiNascita) {
		
		dirigente.setCodiceFiscale(codiceFiscale);
		dirigente.setResidenza(residenza);
		dirigente.setNumeroTelefonico(dataDiNascita);
		dirigente.setDataDiNascita(dataDiNascita);

	}
	
	public void addDirettore(Direttore dirigente) {
		
		elencoDirettori.add(dirigente);
		
	}
	
	public void delDirettore(Direttore dirigente){
		
		elencoDirettori.remove(dirigente);
	
	}
	
	public Direttore getDirettoreByCodiceFiscal(String codiceFiscale) {
		Direttore direttore = null;
		for(Direttore d : elencoDirettori) {
			if(codiceFiscale.equals(d.getCodiceFiscale())) {
				direttore = d;
			}
		}
		return direttore;
	}
	
	
	public ArrayList<Direttore> getDuplicatesDirettore(ArrayList<Direttore> listaDirettori, Direttore dir){

		ArrayList<Direttore> listaDire = new ArrayList<Direttore>();
		
		for(Direttore dire : listaDirettori) {
			
			String s =  dire.getCodiceFiscale();
			
			if(s.equals(dir.getCodiceFiscale())) {
				listaDire.add(dire);
			}
			
		}
		
		return listaDire;
	}
	
	
	public ArrayList<ArrayList<Direttore>> getAllDuplicates(ArrayList<Direttore> listDirettori){
		
		ArrayList<ArrayList<Direttore>> listaDuplicati = new ArrayList<ArrayList<Direttore>>();
		
		HashSet<String> noDuplicati = new HashSet<String>();
		
		for(Direttore direttore :  listDirettori) {
			
			noDuplicati.add(direttore.getCodiceFiscale());
			
		}
		
		for(String s : noDuplicati) {
			Direttore direttore = getDirettoreByCodiceFiscal(s);
			if(getDuplicatesDirettore(listDirettori, direttore).size() >1) {	
				listaDuplicati.add(getDuplicatesDirettore(listDirettori, direttore));
			}
		}

		return listaDuplicati;
	}
				
////////////////ARRAYLIST E METODI DIPENDENTI///////////////
	
	
	List<Dipendente> elencoDipendenti = new ArrayList<Dipendente>();
	
	public List<Dipendente> getElencoDipendenti(){
		
		return (elencoDipendenti);
		
	}
	
	public void  setDipendente(Dipendente dipe,String codiceFiscale, String residenza, String numeroTelefonico){
		
		dipe.setCodiceFiscale(codiceFiscale);
		dipe.setResidenza(residenza);
		dipe.setNumeroTelefonico(numeroTelefonico);
		
	}
	
	public  void addDipendente(Dipendente dipe) { //DA MODIFCARE
		
		elencoDipendenti.add(dipe);
		
	}
	
	public void delDipendente(Dipendente dipe) {
		
		elencoDipendenti.remove(dipe);
		
	}
	
	
	/*public Boolean getDipendentiDoppi(List<Dipendente> listaDipendenti) {
		
		List<String> dipendentiPresi = new ArrayList<String>();
		
		for (Dipendente dipendente : elencoDipendenti) {
			
			String cFiscale = dipendente.getCodiceFiscale();
			
			for(String ctrlCF : dipendentiPresi) {
				
				if(ctrlCF.equals(cFiscale)) {
					
					return true;
					
				}
				
			}dipendentiPresi.add(cFiscale);
			
		}
	
		return false;
		
	} */

	//////////////////ARRAYLIST E METODI OFFICINE////////////////////////
	
	 List<Officina> elencoOfficine = new ArrayList<Officina>();
	
	public  List<Officina> getElencoofficine(){
		
		return (elencoOfficine);
		
	}
	
	public void setOfficina(Officina off,String nome,String indirizzo, int numLavoratori,Direttore dire){
			
		off.setNome(nome);
		off.setIndirizzo(indirizzo);
		off.setNumeroDipendenti(numLavoratori);
		off.setDirigente(dire);
		
	}
	
	public  void addOfficina(Officina off) {
		
		elencoOfficine.add(off);
		
	}
	
	public  void delOffcina(Officina off) {
		
		elencoOfficine.remove(off);
	
	}
	
	public  Boolean controlloDirigenti(Direttore direttoreX) { 
		
		Boolean risultato;
		
		List<Direttore> direttoriLetti = new ArrayList<Direttore>();
		
		for (Officina off : elencoOfficine) {
			
			Direttore controlloDirOff = off.getDirigente();
			
			for(Direttore direttoreTrovato : direttoriLetti){
								
				if(direttoreTrovato.getCodiceFiscale().equals(controlloDirOff.getCodiceFiscale())){
					
					return risultato =  false;
					
				}
				
			} direttoriLetti.add(controlloDirOff);	
			
	}
		
		
		return risultato =  true;
}
	
	/*public Boolean getOfficineDoppi(List<Officina> listaOfficine) {
		
	List<String> officinePrese = new ArrayList<String>();
		
		for (Officina officina : elencoOfficine) {
			
			String offNome = officina.getNome();
			
			for(String nome : officinePrese) {
				
				if(nome.equals(offNome)) {
					
					return true;
					
				}
				
			}officinePrese.add(offNome);
			
		}
	
		return false;
		
	}
	*/
		
	/////////////////////////////ARRAYLIST E METODI PROPIETARI///////////////////
		
	 List<Propietario> elencoPropietari = new ArrayList<Propietario>();
	
	
		public  List<Propietario> getElencoPropietari(){
			
			return (elencoPropietari);
			
		}
		
		public  void  setPropietario(Propietario prop,String codiceFiscale,String residenza, String numeroTelefonico){
			
			prop.setCodiceFiscale(codiceFiscale);
			prop.setResidenza(residenza);
			prop.setNumeroTelefonico(numeroTelefonico);
			
		}
		
		public  void addPropietario(Propietario prop) {
			
			elencoPropietari.add(prop);
			
		}
		public  void delPropietario(Propietario prop) {
			
			elencoPropietari.remove(prop);
		
		}
		
	/*	public Boolean getPropietariDoppi(List<Propietario> listaPropietari) {
			
			List<String> propietariPresi = new ArrayList<String>();
			
			for (Propietario propietario : elencoPropietari) {
				
				String cFiscale = propietario.getCodiceFiscale();
				
				for(String ctrlCF : propietariPresi) {
					
					if(ctrlCF.equals(cFiscale)) {
						
						return true;
						
					}
					
				}propietariPresi.add(cFiscale);
				
			}
		
			return false;
			
		}*/
	
	/////////////////////////////ARRAYLIST E METODI VEICOLI///////////////////
	
	 List<Veicolo> elencoVeicoli = new ArrayList<Veicolo>();
	
	
	public  List<Veicolo> getElencoVeicoli(){
		
		return (elencoVeicoli);
		
	}
	
	public  void setVeicoli(Veicolo veic, String tipo, String modello, String targa, String annoImmatricolazione, Propietario propietario){
		
		veic.setTipo(tipo);
		veic.setModello(modello);
		veic.setTarga(targa);
		veic.setAnnoImmatricolazione(annoImmatricolazione);
		veic.setPropietario(propietario);
	}
	
	public void addVeicolo(Veicolo veic) {
		
		elencoVeicoli.add(veic);
		
	}
	public  void delVeicolo(Veicolo veic) {
		
		elencoVeicoli.remove(veic);
	
	}

 /*	public Boolean getVeicoliDoppi(List<Veicolo> listaVeicoli) {
		
		List<String> veicoliTrovati = new ArrayList<String>();
		
		for(Veicolo veicolo : elencoVeicoli) {
			
			String targaVeicolo = veicolo.getTarga();
			
			for(String s : veicoliTrovati) {
				
				if(s.equals(targaVeicolo)) {
					return true;
				}
				
			}veicoliTrovati.add(targaVeicolo);
			
		}return false;
		
	} */
	
	////////////////////////////ARRAYLIST E METODI FATTURE/////////////////////////

	 ArrayList<Fattura> elencoFatture =  new ArrayList<Fattura>();
	
	 public  ArrayList<Fattura> getElencoFatture(){
		
		return (elencoFatture);
		
	}
	
	public  void  setFatture(Fattura fatt,String dataRilascio, List<Dipendente> dipendentiAnnessi, Dipendente dipendenteFattura, double costo){
		
		fatt.setDataRilascio(dataRilascio);
		fatt.setDipendentiAnnessi((ArrayList<Dipendente>) dipendentiAnnessi);
		fatt.setDipendenteFattura(dipendenteFattura);
		fatt.setCosto(costo);
	}
	
	public  void addFattura(Fattura fatt) {
		
		elencoFatture.add(fatt);
		
	}
	public  void delFattura(Fattura fatt) {
		
		elencoFatture.remove(fatt);
	
	}

	////////////////////////////ARRAYLIST E METODI RIPARAZIONI/////////////////////////
	
	
	 ArrayList<Riparazione> elencoRiparazioni =  new ArrayList<Riparazione>();

	 public  ArrayList<Riparazione> getElencoRiparazioni(){
		
		return (elencoRiparazioni);
		
	 }
	
		public  void  setRiparazione(Riparazione rip,int codice, Officina officina, Veicolo veicolo, String accettazione, Fattura fattura){
				rip.setCodice(codice);
				rip.setOfficina(officina);
				rip.setVeicolo(veicolo);
				rip.setAccettazione(accettazione);
				rip.setFattura(fattura);
				
		}
	
	public void addRiparazione(Riparazione rip) {
		
	elencoRiparazioni.add(rip);
		
	}
	public  void delRiparazione(Riparazione rip) {
		
		elencoRiparazioni.remove(rip);
	
	}
	
	/////////////////ARRAY E METODI CONTRATTI DIPENDENTI////////////////
	
	 ArrayList<ContrattoDip> contrattiDipendenti = new ArrayList<ContrattoDip>();
	 
	public  ArrayList<ContrattoDip> getContrattiDipendenti(){
		
		return contrattiDipendenti;
		
	}
	
	public  void  setContrattiDipendenti(ContrattoDip cDipe,Dipendente dipendente, String dataInizio, String dataFine,Officina officina){
		
		cDipe.setDip(dipendente);
		cDipe.setDataInizio(dataInizio);
		cDipe.setDataInizio(dataInizio);
		cDipe.setOfficina(officina);
		
	}
	
	public  void addContrattoDip(ContrattoDip cDIpe) {
		
		contrattiDipendenti.add(cDIpe);
		
	}
	public  void delContrattoDipendente(ContrattoDip cDipe) {
		
		contrattiDipendenti.remove(cDipe);
	
	}
	
	/////////////////ARRAY E METODI CONTRATTI DIRETTORI////////////////
	
	ArrayList<ContrattoDire> contrattiDirettori = new ArrayList<ContrattoDire>();
	
	public  List<ContrattoDire> getContrattiDirigenti(){
		
		return contrattiDirettori;
		
	}
	
	public  void  setContrattiDirettori(ContrattoDire cDire, Direttore direttore, String dataInizio, String dataFine,Officina officina){
		
		cDire.setDirettore(direttore);
		cDire.setDataInizio(dataInizio);
		cDire.setDataFine(dataFine);
		cDire.setOfficina(officina);
	}
	
	public  void addContrattoDirettore(ContrattoDire cDire) {
	
		contrattiDirettori.add(cDire);
		
	}
	public  void delContrattoDirettore(ContrattoDire cDire) {
		
		contrattiDirettori.remove(cDire);
	
	}
	
	
	public  List<Officina> getOfficineLavorate(Dipendente dip) { // il metodo mi restituirą una lista di tipo officina e si aspetta un input dipendente 
	
		 List<Officina> risultatoOfficina = new ArrayList<Officina>(); // creazione della lista dove verranno inserite le officine 
		
		 for(ContrattoDip cont : contrattiDipendenti) { // per ogni cont di tipo contratto contenuto nella lista contrattiDipendenti fai : 
			
			String codFiscale = cont.getDip().getCodiceFiscale();// una variabile codFiscale di tipo stringa contenente il codice fiscale preso da un dipendente contenente nel in un contratto
			 
			 if(codFiscale.equals(dip.getCodiceFiscale())) {// se il codice fiscale equivale al codice fiscale che stiamo ricercando
				 
				 Officina offTrovata = cont.getOfficina(); // crea una variabile di tipo officina offTrovata contenente l'officina del contratto relativo al codice fiscale trovato
				 risultatoOfficina.add(offTrovata);	// aggiunge alla lista di oggetti officina l'officina trovata
			 }
		 
		 }
		 
		 return risultatoOfficina; // restituisce la liste di officine
		 
	}
	
	
		public List<String> getVeicoliOfficina(Officina off){

		List<Officina> officineCercate = new ArrayList<Officina>();
		List<String> veicoliRiparati = new ArrayList<String>();
		
		for(Riparazione rip : elencoRiparazioni){
			
			Officina offTrovata = rip.getOfficina();
			officineCercate.add(offTrovata);
			
			if(offTrovata.getNome().equals(off.getNome())) {
				
				for(Riparazione ripa : elencoRiparazioni) {
					
					String modVeicolo = ripa.getVeicolo().getModello();
					veicoliRiparati.add(modVeicolo);
				}
			 }
		  }
		
		return veicoliRiparati;
		
		}
		

		public Boolean findDuplicates(List<Object> lista) {
				
				ArrayList<String> visited = new ArrayList<String>();
				
				for(Object f : lista) {
					String fattu = f.toString();
					
					for(String s : visited) {
						if(s.equals(fattu)) {
							return true;
						}
						
					}
					if(!visited.contains(fattu)) {
						visited.add(fattu);
					}
					
				}
				
				return false;
				
			}
			
		
	
		
		
		public ArrayList<ArrayList<String>> getAllDuplicates(List<Direttore> lista){// DA FINIRE - DA FINIRE - DA FINIRE
			
			ArrayList<String> foundedCodFiscale = new ArrayList<String>();
 			ArrayList<String> codiciDoppi = new ArrayList<String>();
 			ArrayList<ArrayList<String>> listaArray = new ArrayList<ArrayList<String>>();
 		
 			for(Direttore dire : lista ) {
 				
 				String cfiscale = dire.getCodiceFiscale();
 				foundedCodFiscale.add(cfiscale);
 				 
 				
 			}
 			
 			return listaArray;
			
		}

			
		
}// chiusura classe gestione

