package model;

public class ContrattoDip {

	private Dipendente dipendente;
	private String dataInizio;
	private String dataFine;
	private Officina officina;
	
	
	
	public ContrattoDip(Dipendente dipendente, String dataInizio, String dataFine,Officina officina) {
		this.dipendente = dipendente;
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
		this.officina = officina;
	}
	
	
	public Dipendente getDip() {
		return dipendente;
	}
	public void setDip(Dipendente dipendente) {
		this.dipendente = dipendente;
	}
	public String getDataInizio() {
		return dataInizio;
	}
	public void setDataInizio(String dataInizio) {
		this.dataInizio = dataInizio;
	}
	public String getDataFine() {
		return dataFine;
	}
	public void setDataFine(String dataFine) {
		this.dataFine = dataFine;
	}
	public Officina getOfficina() {
		return officina;
	}
	public void setOfficina(Officina officina) {
		this.officina = officina;
	}
	
}
