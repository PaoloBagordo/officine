package model;

public abstract class Lavoratore {

	private String codiceFiscale;
	private String residenza;
	private String numeroTelefonico;
	
	
	public Lavoratore () {}
	
	public Lavoratore(String codiceFiscale, String residenza, String numeroTelefoico) {
		this.codiceFiscale = codiceFiscale;
		this.residenza = residenza;
		this.numeroTelefonico = numeroTelefoico;
	}
	
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	public String getResidenza() {
		return residenza;
	}
	public void setResidenza(String residenza) {
		this.residenza = residenza;
	}
	public String getNumeroTelefonico() {
		return numeroTelefonico;
	}
	public void setNumeroTelefonico(String numeroTelefonico) {
		this.numeroTelefonico = numeroTelefonico;
	}
	
	
	
}
