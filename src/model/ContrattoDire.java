package model;

public class ContrattoDire {

	private Direttore direttore;
	private String dataInizio;
	private String dataFine;
	private Officina officina;
	
	
	
	public ContrattoDire(Direttore direttore, String dataInizio, String dataFine,Officina officina) {
		this.direttore = direttore;
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
		this.officina = officina;
	}
	
	
	public Direttore getDirettore() {
		return direttore;
	}
	public void setDirettore(Direttore direttore) {
		this.direttore = direttore;
	}
	public String getDataInizio() {
		return dataInizio;
	}
	public void setDataInizio(String dataInizio) {
		this.dataInizio = dataInizio;
	}
	public String getDataFine() {
		return dataFine;
	}
	public void setDataFine(String dataFine) {
		this.dataFine = dataFine;
	}
	public Officina getOfficina() {
		return officina;
	}
	public void setOfficina(Officina officina) {
		this.officina = officina;
	}
	
}
