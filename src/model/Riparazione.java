package model;

import java.util.Date;

public class Riparazione {

	private int codice;
	private Officina officina;
	private Veicolo veicolo;
	private String accettazione;
	private Fattura fattura;
	
	public Riparazione(int codice, Officina officina, Veicolo veicolo, String accettazione, Fattura fattura) {
		this.codice = codice;
		this.officina = officina;
		this.veicolo = veicolo;
		this.accettazione = accettazione;
	}
	

	public int getCodice() {
		return codice;
	}

	public void setCodice(int codice) {
		this.codice = codice;
	}

	public Officina getOfficina() {
		return officina;
	}

	public void setOfficina(Officina officina) {
		this.officina = officina;
	}

	public Veicolo getVeicolo() {
		return veicolo;
	}

	public void setVeicolo(Veicolo veicolo) {
		this.veicolo = veicolo;
	}

	public String getAccettazione() {
		return accettazione;
	}

	public void setAccettazione(String accettazione) {
		this.accettazione = accettazione;
	}


	public Fattura getFattura() {
		return fattura;
	}


	public void setFattura(Fattura fattura) {
		this.fattura = fattura;
	}
	
}
