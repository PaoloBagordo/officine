package model;

import java.util.Date;

public class Direttore extends Lavoratore{

	private String dataDiNascita;

	
	public Direttore(String codiceFiscale, String residenza, String numeroDiTelefono, String dataDiNascita) {
		
		super(codiceFiscale, residenza, numeroDiTelefono);
		this.dataDiNascita = dataDiNascita;
		
		
	};

	public String getDataDiNascita() {
		return dataDiNascita;
	}

	public void setDataDiNascita(String dataDiNascita) {
		this.dataDiNascita = dataDiNascita;
	}
	
	
	
}
