package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Fattura {

	private String dataRilascio;
	private List<Dipendente> dipendentiAnnessi;
	private Dipendente dipendenteFattura;
	private double costo;
	
	public Fattura(String dataRilascio, List<Dipendente> dipendentiAnnessi, Dipendente dipendenteFattura, double costo) {
		this.dataRilascio = dataRilascio;
		this.dipendentiAnnessi = dipendentiAnnessi;
		this.dipendenteFattura = dipendenteFattura;
		this.costo = costo;
	}
	
	
	public Fattura() {};
	
	public String getDataRilascio() {
		return dataRilascio;
	}
	public void setDataRilascio(String dataRilascio) {
		this.dataRilascio = dataRilascio;
	}
	public List<Dipendente> getDipendentiAnnessi() {
		return dipendentiAnnessi;
	}
	public void setDipendentiAnnessi(ArrayList<Dipendente> dipendentiAnnessi) {
		this.dipendentiAnnessi = dipendentiAnnessi;
	}
	public Dipendente getDipendenteFattura() {
		return dipendenteFattura;
	}
	public void setDipendenteFattura(Dipendente dipendenteFattura) {
		this.dipendenteFattura = dipendenteFattura;
	}
	public double getCosto() {
		return costo;
	}
	public void setCosto(double costo) {
		this.costo = costo;
	}
	
	
	
	
	
}
