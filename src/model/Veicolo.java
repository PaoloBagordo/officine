package model;

public class Veicolo {

	private String tipo;
	private String modello;
	private String targa;
	private String annoImmatricolazione;
	Propietario propietario;
	
	
	public Veicolo(String tipo, String modello, String targa, String annoImmatricolazione, Propietario propietario) {
		this.tipo = tipo;
		this.modello = modello;
		this.targa = targa;
		this.annoImmatricolazione = annoImmatricolazione;
		this.propietario = propietario;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getModello() {
		return modello;
	}


	public void setModello(String modello) {
		this.modello = modello;
	}


	public String getTarga() {
		return targa;
	}


	public void setTarga(String targa) {
		this.targa = targa;
	}


	public String getAnnoImmatricolazione() {
		return annoImmatricolazione;
	}


	public void setAnnoImmatricolazione(String annoImmatricolazione) {
		this.annoImmatricolazione = annoImmatricolazione;
	}


	public Propietario getPropietario() {
		return propietario;
	}


	public void setPropietario(Propietario propietario) {
		this.propietario = propietario;
	}
	


	
	
	
	
}
