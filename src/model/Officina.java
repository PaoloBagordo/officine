package model;

public class Officina {
		
	private String nome;
	private String indirizzo;
	private int numeroDipendenti;
	private Direttore dirigente;
	

	public Officina(String unNome, String indirizzo, int numeroDipendenti, Direttore dirigente) {
		this.nome = unNome;
		this.indirizzo = indirizzo;
		this.numeroDipendenti = numeroDipendenti;
		this.dirigente = dirigente;
	}
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public int getNumeroDipendenti() {
		return numeroDipendenti;
	}
	public void setNumeroDipendenti(int numeroDipendenti) {
		this.numeroDipendenti = numeroDipendenti;
	}
	public Direttore getDirigente() {
		return dirigente;
	}
	public void setDirigente(Direttore dirigente) {
		this.dirigente = dirigente;
	}
	
	
		
}
