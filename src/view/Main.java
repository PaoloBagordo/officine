package view;

import java.util.ArrayList;
import java.util.List;

import controller.Gestione;
import model.ContrattoDip;
import model.ContrattoDire;
import model.Dipendente;
import model.Direttore;
import model.Fattura;
import model.Officina;
import model.Propietario;
import model.Riparazione;
import model.Veicolo;

public class Main {
public static void main(String[] args) {
	
Gestione ctrl = new Gestione();
		
	///////////////////SEZIONE DIRETTORI///////////////////

	Direttore dir1 = new Direttore("FLCSDR70L06L219P", "Torino via nizza 44", "44447776", "17-01-1970" );
	Direttore dir2 = new Direttore("BDDSRG46A07L219J", "Torino sergio coppola", "22233344", "10-09-1990");
	Direttore dir3 = new Direttore("SNDFLL80A01L219G", "Torino antonio mango", "4444444444", "23-04-1987");

	 List<Direttore> stampaListaDirettori = ctrl.getElencoDirettori();
	 ctrl.addDirettore(dir1);
	 ctrl.addDirettore(dir1);
	 ctrl.addDirettore(dir2);
	 ctrl.addDirettore(dir2);
	 ctrl.addDirettore(dir3);
	 ctrl.addDirettore(dir3);
	 ctrl.addDirettore(dir3);
	 
	 ctrl.getDuplicatesDirettore(ctrl.getElencoDirettori(), ctrl.getElencoDirettori().get(5));
	
	 
	///////////////////SEZIONE DIPENDENTI///////////////////
	 
	 	Dipendente dip1 = new Dipendente("FBNLCU80A01L219Y","Torino via luini 55", "3334445553");
		Dipendente dip2 = new Dipendente("PLAGLI95E06A124Q","Torino corso ateneo 7", "345456473");
		Dipendente dip3 = new Dipendente("CCCPTC78M08L219N","Torino edoardo fieri 13", "3276665333");
		Dipendente dip4 = new Dipendente("GAIFMM80D01L219D","Torino Cuneo 33", "33333333");
		Dipendente dip5 = new Dipendente("FRRLXA80A01L219C","Torino Via dei parchi 44", "44444444");
		Dipendente dip6 = new Dipendente("GLLNRN72A03L219Z","Torino Via gaialli 65", "5555666565");
		Dipendente dip7 = new Dipendente("BRNDIO01A01L219K","Torino Corso cagliari", "999444333");
		Dipendente dip8 = new Dipendente("JSTJNN80A01D205G","Torino via rieti 44", "332225553");
		Dipendente dip9 = new Dipendente("RFFMTT80A01F205K","Torino via luini 55", "3336665753");
		
	 List<Dipendente> stampaListaDipendenti = ctrl.getElencoDipendenti();
	 ctrl.addDipendente(dip1);
	 ctrl.addDipendente(dip1);
	 ctrl.addDipendente(dip2);
	 ctrl.addDipendente(dip3);
	 ctrl.addDipendente(dip4);
	 ctrl.addDipendente(dip5);
	 ctrl.addDipendente(dip6);
	 ctrl.addDipendente(dip7);
	 ctrl.addDipendente(dip8);
	 ctrl.addDipendente(dip9);
	 ctrl.setDipendente(dip6, "","","");
	 ctrl.delDipendente(dip9);
	 
	// System.out.println(ctrl.getDuplicates(ctrl.getElencoDipendenti()));
	 	 
	 ///////////////////SEZIONE OFFICINE///////////////////
	 
	 
	 Officina off1 = new Officina("Metlamania", "Torino ,Via berti 44", 25, dir1);
	 Officina off2 = new Officina("Meario hardwere custom Shop", "Alba ,Corso Europa 33", 25, dir2);
	 Officina off20 = new Officina("Meario hardwere custom Shop", "Alba ,Corso Europa 33", 25, dir2);
	 Officina off3 = new Officina("Easy riparo", "Canale, via roma 24", 25, dir1);
	
	 List<Officina> stampaListaOfficine = ctrl.getElencoofficine();
	 ctrl.addOfficina(off1);
	 ctrl.addOfficina(off2);
	 ctrl.addOfficina(off20);
	 ctrl.addOfficina(off3);
	 ctrl.setOfficina(off1, "Officina mario", "Via x n4", 12, dir2);
	 ctrl.delOffcina(off3);
	 
	 ctrl.controlloDirigenti(dir3);
	// System.out.println(ctrl.getDuplicates(ctrl.getElencoofficine()));
	 	 
	 ///////////////////SEZIONE PROPIETARI///////////////////

		 Propietario prop1 = new Propietario("FBNLCU80A01L219Y","Torino via luini 55", "3334445553");
		 Propietario prop2 = new Propietario("PLAGLI95E06A124Q","Torino corso ateneo 7", "345456473");
		 Propietario prop3 = new Propietario("CCCPTC78M08L219N","Torino edoardo fieri 13", "3276665333");

		 List<Propietario> stampaListaPropietati = ctrl.getElencoPropietari();
		 ctrl.addPropietario(prop1);
		 ctrl.addPropietario(prop2);
		 ctrl.addPropietario(prop3);
		 ctrl.setPropietario(prop3, "","","");
		 ctrl.delPropietario(prop3);
		 
		// System.out.println(ctrl.getDuplicates(ctrl.getElencoPropietari()));
	
	 ///////////////////SEZIONE VEICOLI///////////////////
	 

	 Veicolo veic1 = new Veicolo("Automobile","Bmw serie1", "AB 000CD", "2011", prop1);
	 Veicolo veic2 = new Veicolo("Automobile","Ford fiesta", "CN 345FD", "2015", prop2);
	 Veicolo veic3 = new Veicolo("Automobile","Fiat punto", "GG 555FF", "2017", prop3);
		
	 List<Veicolo> stampaListaVeicoli = ctrl.getElencoVeicoli();
	 ctrl.addVeicolo(veic1);
	 ctrl.addVeicolo(veic2);
	 ctrl.addVeicolo(veic3);
	 ctrl.setVeicoli(veic2, "","","","",prop2);
	 ctrl.delVeicolo(veic3);
	 	 
	 ///////////////////SEZIONE FATTURE///////////////////
	 
	
	 Dipendente dipFattura = new Dipendente("", "", "");
	 
	 List<Dipendente> dipendentiCorrelati = new ArrayList<Dipendente>();
	

	 
	 Fattura fatt1 = new Fattura("", dipendentiCorrelati, dipFattura, 400.99);
	 Fattura fatt2 = new Fattura("", dipendentiCorrelati, dipFattura, 667.00);
	 Fattura fatt3 = new Fattura("", dipendentiCorrelati, dipFattura, 100000.00);
	
	 
	 ArrayList<Fattura> stampaListaFatture = ctrl.getElencoFatture();
	 ctrl.addFattura(fatt1);
	 ctrl.addFattura(fatt1);
	 ctrl.addFattura(fatt2);
	 ctrl.addFattura(fatt3);
	 ctrl.delFattura(fatt3);
	 
	// System.out.println(ctrl.getDuplicates(ctrl.getElencoFatture()));
	 
	 ///////////////////SEZIONE RIPARAZIONI///////////////////
	 Riparazione rip1 = new Riparazione(1, off1, veic1, "21-01-2022", fatt1);
	 Riparazione rip2 = new Riparazione(2, off2, veic2, "10-02-2022", fatt2);
	 Riparazione rip3 = new Riparazione(9, off3, veic3, "10-02-2022", fatt1);
	 Riparazione rip4 = new Riparazione(3, off2, veic1, "10-02-2022", fatt1);
	 
	 
	 List<Riparazione> stampaListaRiparazione = ctrl.getElencoRiparazioni();
	 ctrl.addRiparazione(rip1);
	 ctrl.addRiparazione(rip1);
	 ctrl.addRiparazione(rip2);
	 ctrl.addRiparazione(rip3);
	 ctrl.setRiparazione(rip3, 3, off3, veic3, "11-02-2022", fatt3);
	 ctrl.delRiparazione(rip3);
	 
	// System.out.println(ctrl.getDuplicates(ctrl.getElencoRiparazioni()));
	 	 
	 ctrl.getVeicoliOfficina(off2);
	 
	 ///////////////////SEZIONE CONTRATTI DIPENDENTI///////////////////
	 
	 ContrattoDip cont1 = new ContrattoDip(dip1, "12/01/2018", "", off1); 
	 ContrattoDip cont2 = new ContrattoDip(dip2, "12/01/2018", "", off1); 
	 ContrattoDip cont3 = new ContrattoDip(dip3, "12/01/2018", "", off1); 
	 ContrattoDip cont4 = new ContrattoDip(dip4, "12/01/2018", "", off2); 
	 ContrattoDip cont5 = new ContrattoDip(dip5, "12/01/2018", "", off2); 
	 ContrattoDip cont6 = new ContrattoDip(dip6, "12/01/2018", "", off2); 
	 ContrattoDip cont7 = new ContrattoDip(dip7, "12/01/2018", "", off3); 
	 ContrattoDip cont8 = new ContrattoDip(dip8, "12/01/2018", "", off3); 
	 ContrattoDip cont9 = new ContrattoDip(dip9, "12/01/2018", "", off3); 
	 
	 ArrayList<ContrattoDip> stampaListaContratti = ctrl.getContrattiDipendenti();
	 ctrl.addContrattoDip(cont1);
	 //ctrl.addContrattoDip(cont1);
	 ctrl.addContrattoDip(cont2);
	 ctrl.addContrattoDip(cont3);
	 ctrl.addContrattoDip(cont4);
	 ctrl.addContrattoDip(cont5);
	 ctrl.addContrattoDip(cont6);
	 ctrl.addContrattoDip(cont7);
	 ctrl.addContrattoDip(cont8);
	 ctrl.addContrattoDip(cont9);
	 ctrl.setContrattiDipendenti(cont9,dip9,"12/01/2018", "10/02/2022", off3);
	 ctrl.delContrattoDipendente(cont9);
	 
	// System.out.println(ctrl.getDuplicates(ctrl.getContrattiDipendenti()));
	 
	///////////////////SEZIONE CONTARTTI DIRIGENTI///////////////////
	 ContrattoDire cont1D = new ContrattoDire(dir1, "12/01/2018", "", off1); 
	 ContrattoDire cont2D = new ContrattoDire(dir2, "12/01/2018", "", off1); 
	 ContrattoDire cont3D = new ContrattoDire(dir3, "12/01/2018", "", off1); 
	 
	 List<ContrattoDire> stampaListaContrattiDire = ctrl.getContrattiDirigenti();
	 ctrl.addContrattoDirettore(cont1D);
	 ctrl.addContrattoDirettore(cont2D);
	 ctrl.addContrattoDirettore(cont3D);
	 ctrl.setContrattiDirettori(cont3D, dir3, "12/01/2018", "12/01/2022", off3);
	 ctrl.delContrattoDirettore(cont3D);
	 
	// System.out.println(ctrl.getDuplicates(ctrl.getContrattiDirigenti()));
	
	 ctrl.getOfficineLavorate(dip7);
	 
 }	
}
